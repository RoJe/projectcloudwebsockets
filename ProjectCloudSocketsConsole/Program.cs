﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace ProjectCloudSocketsConsole
{
    class MainClass
    {

        static Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
        static private string guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
        static SHA1 sha1 = SHA1CryptoServiceProvider.Create();

        public static void Main(string[] args)
        {
            Console.WriteLine("Started SocketAPI...");
            Console.WriteLine("Created By Maikel Witlox & Robbert de Jel");
            ThreadPool.QueueUserWorkItem(CreateSocket, null);
            //ThreadPool.QueueUserWorkItem(MakeFakeData, null);
            Console.WriteLine("Queued threads... Going into main while loop to keep system alive.");
            while (true)
            {
                Thread.Sleep(10000);
                Console.WriteLine("Arbitrary writeline to stop the OS from freaking out...");
            }
        }
        private static void MakeFakeData(object obj)
        {
            while (true)
            {
                try
                {
                    WebResponse webRequest = WebRequest.Create("https://ehealthapi2r-fun.azurewebsites.net/api/Sensors/AddMockData").GetResponse();
                }catch(Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                
                Thread.Sleep(2000);
            }

        }

        private static void CreateSocket(object obj)
        {
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, 8080));
            serverSocket.Listen(2147483647);
            serverSocket.BeginAccept(null, 0, OnAccept, null);
        }

        private static void OnAccept(IAsyncResult result)
        {
            ConnectClient(result);

        }

        private static void ThreadedConnection(object obj)
        {
            var buffer = new byte[1024];
            var client = (Socket)obj;
            var i = client.Receive(buffer);
            string headerResponse = Encoding.UTF8.GetString(buffer).Substring(0, i);

            try
            {
                if (client != null)
                {
                    /* Handshaking and managing ClientSocket */
                    Console.WriteLine(headerResponse);
                    if (headerResponse.ToCharArray().Length > buffer.Length || headerResponse.ToCharArray().Length <= 0)
                    {
                        return;
                    }

                    var key = headerResponse.Replace("ey:", "`");
                    if (key.Split('`').Length > 1)
                    {
                        key = key.Split('`')[1];
                    }
                    key = key.Replace("\r", "");
                    if (key.Split('\n').Length > 0)
                    {
                        key = key.Split('\n')[0];
                    }

                    key = key.Trim();

                    // key should now equal dGhlIHNhbXBsZSBub25jZQ==
                    var test1 = AcceptKey(ref key);

                    var newLine = "\r\n";

                    var response = "HTTP/1.1 101 Switching Protocols" + newLine
                         + "Upgrade: websocket" + newLine
                         + "Connection: Upgrade" + newLine
                         + "Sec-WebSocket-Accept: " + test1 + newLine + newLine
                         //+ "Sec-WebSocket-Protocol: chat, superchat" + newLine
                         //+ "Sec-WebSocket-Version: 13" + newLine
                         ;

                    client.Send(System.Text.Encoding.UTF8.GetBytes(response));
                    while (true && client != null && client.Connected)
                    {
                        var message = client.Receive(buffer); // wait for client to send a message
                        string decodedMessage = GetDecodedData(buffer, message);

                        if (decodedMessage == "") // Sockets are weird sometimes
                        {
                            return;
                        }
                        HandleMessage(decodedMessage, client);

                    }
                }
            }
            catch (SocketException exception)
            {
                Console.WriteLine(exception.Message);
            }
            finally
            {
                Console.WriteLine("Killing thread..");
            }
        }

        private static void HandleMessage(string msg, Socket client)
        {
            switch (msg)
            {
                case "Sensordata":
                    client.Send(GetFrameFromString(GetSensorData()));
                    break;
                case "test2":
                    client.Send(GetFrameFromString("This is responce from test2"));
                    break;
                default:
                    break;
            }
        }

        private static String GetSensorData()
        {
            String heathBeat = (new Random().Next(100, 160)).ToString();
            String temperature = (new Random().Next(28, 42)).ToString();
            String breathFreq = (new Random().Next(15, 26)).ToString();
            String saturation = (new Random().Next(61, 96)).ToString();

            return heathBeat + "," + temperature + "," + breathFreq + "," + saturation;
        }

        private static void ConnectClient(IAsyncResult result)
        {
            try
            {
                if (serverSocket != null && serverSocket.IsBound)
                {
                    Socket client = serverSocket.EndAccept(result);
                    ThreadPool.QueueUserWorkItem(ThreadedConnection, client);
                }
            }
            catch (SocketException exception)
            {
                throw exception;
            }
            finally
            {
                if (serverSocket != null && serverSocket.IsBound)
                {
                    serverSocket.BeginAccept(null, 0, OnAccept, null);
                }
            }
        }



        public static T[] SubArray<T>(T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        private static string AcceptKey(ref string key)
        {
            string longKey = key + guid;
            byte[] hashBytes = ComputeHash(longKey);
            return Convert.ToBase64String(hashBytes);
        }

        
        private static byte[] ComputeHash(string str)
        {
            return sha1.ComputeHash(System.Text.Encoding.ASCII.GetBytes(str));
        }

        //Needed to decode frame
        public static string GetDecodedData(byte[] buffer, int length)
        {
            byte b = buffer[1];
            int dataLength = 0;
            int totalLength = 0;
            int keyIndex = 0;

            if (b - 128 <= 125)
            {
                dataLength = b - 128;
                keyIndex = 2;
                totalLength = dataLength + 6;
            }

            if (b - 128 == 126)
            {
                dataLength = BitConverter.ToInt16(new byte[] { buffer[3], buffer[2] }, 0);
                keyIndex = 4;
                totalLength = dataLength + 8;
            }

            if (b - 128 == 127)
            {
                dataLength = (int)BitConverter.ToInt64(new byte[] { buffer[9], buffer[8], buffer[7], buffer[6], buffer[5], buffer[4], buffer[3], buffer[2] }, 0);
                keyIndex = 10;
                totalLength = dataLength + 14;
            }

            if (totalLength > length)
                return "";

            byte[] key = new byte[] { buffer[keyIndex], buffer[keyIndex + 1], buffer[keyIndex + 2], buffer[keyIndex + 3] };

            int dataIndex = keyIndex + 4;
            int count = 0;
            for (int i = dataIndex; i < totalLength; i++)
            {
                buffer[i] = (byte)(buffer[i] ^ key[count % 4]);
                count++;
            }
            if (dataLength < 0) // When a sockets disconnects it can send some weird shit
                return "";

            return Encoding.ASCII.GetString(buffer, dataIndex, dataLength);
        }

        //function to create  frames to send to client 
        /// <summary>
        /// Enum for opcode types
        /// </summary>
        public enum EOpcodeType
        {
            /* Denotes a continuation code */
            Fragment = 0,

            /* Denotes a text code */
            Text = 1,

            /* Denotes a binary code */
            Binary = 2,

            /* Denotes a closed connection */
            ClosedConnection = 8,

            /* Denotes a ping*/
            Ping = 9,

            /* Denotes a pong */
            Pong = 10
        }

        /// <summary>Gets an encoded websocket frame to send to a client from a string</summary>
        /// <param name="Message">The message to encode into the frame</param>
        /// <param name="Opcode">The opcode of the frame</param>
        /// <returns>Byte array in form of a websocket frame</returns>
        public static byte[] GetFrameFromString(string Message, EOpcodeType Opcode = EOpcodeType.Text)
        {
            byte[] response;
            byte[] bytesRaw = Encoding.Default.GetBytes(Message);
            byte[] frame = new byte[10];

            int indexStartRawData = -1;
            int length = bytesRaw.Length;

            frame[0] = (byte)(128 + (int)Opcode);
            if (length <= 125)
            {
                frame[1] = (byte)length;
                indexStartRawData = 2;
            }
            else if (length >= 126 && length <= 65535)
            {
                frame[1] = (byte)126;
                frame[2] = (byte)((length >> 8) & 255);
                frame[3] = (byte)(length & 255);
                indexStartRawData = 4;
            }
            else
            {
                frame[1] = (byte)127;
                frame[2] = (byte)((length >> 56) & 255);
                frame[3] = (byte)((length >> 48) & 255);
                frame[4] = (byte)((length >> 40) & 255);
                frame[5] = (byte)((length >> 32) & 255);
                frame[6] = (byte)((length >> 24) & 255);
                frame[7] = (byte)((length >> 16) & 255);
                frame[8] = (byte)((length >> 8) & 255);
                frame[9] = (byte)(length & 255);

                indexStartRawData = 10;
            }

            response = new byte[indexStartRawData + length];

            int i, reponseIdx = 0;

            //Add the frame bytes to the reponse
            for (i = 0; i < indexStartRawData; i++)
            {
                response[reponseIdx] = frame[i];
                reponseIdx++;
            }

            //Add the data bytes to the response
            for (i = 0; i < length; i++)
            {
                response[reponseIdx] = bytesRaw[i];
                reponseIdx++;
            }

            return response;
        }
    }

}
